'use strict';


const
    Boom = require("boom");

var
    db = require('../../middleware/db.js'),
    modele = require('../../models/customer/model.js');


async function getProductHandler(request, reply) {
    try {
        sequelize_instance = db.make();
        var x = modele.test(sequelize_instance);

        reply('Hello, world!');

    } catch (ex) {
        reply(Boom.create(ex.statusCode, ex.message));
    }

}

exports.register = function (server, options, next){

    server.route({
        method: 'GET',
        path: '/',
        handler: getProductHandler
    });

    next();
}

exports.register.attributes = {
    name: "customers"
};
