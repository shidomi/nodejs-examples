const Sequelize = require('sequelize');


function initDB() {
    const sequelize = new Sequelize('nodejsexamples', 'root', 'shidomi', {
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    });

    return sequelize;

    // sequelize
    //     .authenticate()
    //     .then(() => {
    //         console.log('Connection has been established successfully.');
    //     })
    //     .catch(err => {
    //         console.error('Unable to connect to the database:', err);
    //     });
}

module.exports.make = initDB;
